const cards = document.querySelectorAll(".card-container"); //pentru ca avem nevoie de toate card-urile,de  aceea am folosit querySelectorAll

const cardTitle = document.querySelector(".card-title"); // am folosit querySelector pentru ca am selectat doar un element

cardTitle.style.opacity = "1";

//console.log(cards[4]);

//forEach are nevoie de callback function
cards.forEach(function (card) {
    card.addEventListener("click", function (e) {
        //console.log("something");
        //card.classList.add("active");

        if (!card.classList.contains("active")) {
            cards.forEach((card) => {
                card.classList.remove("active");
                card.childNodes[3].style.opacity = "0";
            });
        }
        card.classList.toggle("active");
        card.childNodes[3].style.opacity = "1";
    });
});
